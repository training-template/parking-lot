package com.oobootcamp.parkinglot.exception;

public class CarNotFoundException extends Exception {

  public CarNotFoundException(final String message) {
    super(message);
  }
}
