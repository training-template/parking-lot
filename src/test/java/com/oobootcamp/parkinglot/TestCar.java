package com.oobootcamp.parkinglot;

public enum TestCar {
  A("A"),
  B("B"),
  DUMMY("DUMMY"),
  ;

  private final String licence;

  TestCar(final String licence) {
    this.licence = licence;
  }

  public Car create() {
    return new Car(licence);
  }
}
