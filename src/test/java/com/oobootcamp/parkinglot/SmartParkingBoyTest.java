package com.oobootcamp.parkinglot;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.Lists;
import com.oobootcamp.parkinglot.exception.NoParkingLotException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;
import com.oobootcamp.parkinglot.parkingstrategy.ParkingStrategy;
import com.oobootcamp.parkinglot.parkingstrategy.SmartParkingStrategy;
import java.util.Optional;
import org.junit.Test;

public class SmartParkingBoyTest {

  /**
   * Given a Parking Boy manages a Parking Lot and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be able to Park a Car and get Receipt
   */
  @Test
  public void test_GivenParkingBoyWithParkingLotAndCar_WhenParkingBoyParkCar_ThenCanParkCarAndGetReceipt()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy(new SmartParkingStrategy());
    final ParkingLot parkingLot = new ParkingLot(1);
    parkingBoy.manage(parkingLot);
    final Car car = TestCar.DUMMY.create();

    final Receipt receipt = parkingBoy.park(car);

    assertThat(receipt.getCarLicence()).isEqualTo("DUMMY");
  }

  /**
   * Given Parking Lot A has 2 available space, Parking Lot B has 1 available space
   * When Smart Parking Strategy chooses target Parking Lot
   * Then Smart Parking Strategy will be able to choose Parking Lot A
   */
  @Test
  public void test_GivenParkingLotAHas2AvailableSpaceAndBHas1AvailableSpaceWhenSmartParkingStrategyChooseTargetParkingLotThenCanChooseParkingLotA() {
    final ParkingLot parkingLotA = new ParkingLot(2, "A");
    final ParkingLot parkingLotB = new ParkingLot(1, "B");
    final ParkingStrategy parkingStrategy = new SmartParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotA);
  }

  /**
   * Given Parking Lot A has 2 available space, Parking Lot B has 3 available space
   * When Smart Parking Strategy chooses target Parking Lot
   * Then Smart Parking Strategy will be able to choose Parking Lot B
   */
  @Test
  public void test_GivenParkingLotAHas2AvailableSpaceAndBHas3AvailableSpace_WhenSmartParkingStrategyChooseTargetParkingLot_ThenCanChooseParkingLotB() {
    final ParkingLot parkingLotA = new ParkingLot(2, "A");
    final ParkingLot parkingLotB = new ParkingLot(3, "B");
    final ParkingStrategy parkingStrategy = new SmartParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotB);
  }

  /**
   * Given Parking Lot A has 3 space, Parking Lot B has 3 space and A Car
   * When Parking Lot A parks a Car
   * Then Smart Parking Strategy will be able to choose Parking Lot B
   */
  @Test
  public void test_Given2ParkingLotAHas3SpaceAndBHas3Space_WhenParkingLotAParksACar_ThenCanChooseParkingLotB()
      throws SameCarParkedException, ParkingLotFullException {
    final ParkingLot parkingLotA = new ParkingLot(3, "A");
    final ParkingLot parkingLotB = new ParkingLot(3, "B");
    final Car car = TestCar.DUMMY.create();
    parkingLotA.park(car);
    final ParkingStrategy parkingStrategy = new SmartParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotB);
  }

  /**
   * Given Parking Lot A, B, C all have same available space
   * When Smart Parking Strategy chooses target Parking Lot
   * Then Smart Parking Strategy will be able to choose Parking Lot A
   */
  @Test
  public void test_GivenAllParkingLotWithSameAvailableSpace_WhenSmartParkingStrategyChooseTargetParkingLot_ThenCanChooseFirstParkingLot() {
    final ParkingLot parkingLotA = new ParkingLot(10, "A");
    final ParkingLot parkingLotB = new ParkingLot(10, "B");
    final ParkingLot parkingLotC = new ParkingLot(10, "C");
    final ParkingStrategy parkingStrategy = new SmartParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB, parkingLotC));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotA);
  }

  /**
   * Given 3 Parking Lot As has 2 available space, Parking Lot B has 3 available space, Parking Lot C has 1 availableSpace
   * When Smart Parking Strategy chooses target Parking Lot
   * Then Smart Parking Strategy will be able to choose Parking Lot B
   */
  @Test
  public void test_GivenMultipleParkingLotWithDifferentAvailableSpace_WhenSmartParkingStrategyChooseTargetParkingLot_ThenCanChooseLargestAvailableParkingLot() {
    final ParkingLot parkingLotA = new ParkingLotStub(10, "A",2);
    final ParkingLot parkingLotB = new ParkingLotStub(10, "B", 3);
    final ParkingLot parkingLotC = new ParkingLotStub(10, "C", 1);
    final ParkingStrategy parkingStrategy = new SmartParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB, parkingLotC));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotB);
  }
}
