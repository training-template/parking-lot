package com.oobootcamp.parkinglot.exception;

public class ParkingLotNotFoundException extends Exception {

  public ParkingLotNotFoundException(final String message) {
    super(message);
  }
}
