package com.oobootcamp.parkinglot;

import static com.oobootcamp.parkinglot.predicate.CarPredicate.isSameLicence;

import com.google.common.collect.Lists;
import com.oobootcamp.parkinglot.exception.CarNotFoundException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;
import java.util.List;

public class ParkingLot {

  private final int maxCapacity;
  private final List<Car> parkedCars;
  private final String name;

  public ParkingLot(final int maxCapacity) {
    this(maxCapacity, "");
  }

  public ParkingLot(final int maxCapacity, final String name) {
    this.name = name;
    this.maxCapacity = maxCapacity;
    this.parkedCars = Lists.newArrayList();
  }

  public Receipt park(final Car car) throws ParkingLotFullException, SameCarParkedException {
    if (isFull()) {
      throw new ParkingLotFullException("Parking Lot is full");
    }

    if (isCarAlreadyParked(car)) {
      throw new SameCarParkedException("Same car cannot be parked twice");
    }

    this.parkedCars.add(car);
    return new Receipt(car.getLicence(), name);
  }

  private boolean isCarAlreadyParked(final Car car) {
    return parkedCars.stream().anyMatch(isSameLicence(car.getLicence()));
  }

  public boolean isFull() {
    return parkedCars.size() >= maxCapacity;
  }

  public List<Car> getParkedCars() {
    return this.parkedCars;
  }

  public Car pick(Receipt receipt) throws CarNotFoundException {
    return parkedCars.stream()
        .filter(isSameLicence(receipt.getCarLicence()))
        .findFirst()
        .map(car -> {
          parkedCars.remove(car);
          return car;
        })
        .orElseThrow(() -> new CarNotFoundException("Car is not found"));
  }

  public String getName() {
    return name;
  }

  public int getAvailableSpace() {
    return maxCapacity - parkedCars.size();
  }

  public double getAvailableSpaceRate() {
    return getAvailableSpace() / (double) maxCapacity;
  }

  protected int getMaxCapacity() {
    return maxCapacity;
  }
}
