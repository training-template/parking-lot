package com.oobootcamp.parkinglot.predicate;

import com.oobootcamp.parkinglot.ParkingLot;
import java.util.function.Predicate;

public class ParkingLotPredicate {

  private ParkingLotPredicate() {

  }

  public static Predicate<ParkingLot> isFull() {
    return ParkingLot::isFull;
  }

  public static Predicate<ParkingLot> isAvailable() {
    return isFull().negate();
  }

  public static Predicate<ParkingLot> isSameName(final String name) {
    return parkingLot -> name.equals(parkingLot.getName());
  }
}
