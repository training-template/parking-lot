package com.oobootcamp.parkinglot;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.Lists;
import com.oobootcamp.parkinglot.parkingstrategy.ParkingStrategy;
import com.oobootcamp.parkinglot.parkingstrategy.SuperParkingStrategy;
import java.util.Optional;
import org.junit.Test;

public class SuperParkingStrategyTest {

  /**
   * Given Parking Lot A has 1 available space
   * When Super Parking Strategy chooses target Parking Lot
   * Then Super Parking Strategy will be able to choose Parking Lot A
   */
  @Test
  public void test_GivenParkingLotAHas2AvailableSpaceAndBHas1AvailableSpaceWhenSuperParkingStrategyChooseTargetParkingLotThenCanChooseParkingLotA() {
    final ParkingLot parkingLotA = new ParkingLot(1, "A");
    final ParkingStrategy parkingStrategy = new SuperParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotA);
  }

  /**
   * Given Parking Lot A has 2 / 10 available space, Parking Lot B has 3 / 10 available space
   * When Super Parking Strategy chooses target Parking Lot
   * Then Super Parking Strategy will be able to choose Parking Lot B
   */
  @Test
  public void test_GivenParkingLotAHas2Of10AvailableSpaceAndBHas3Of10AvailableSpace_WhenSuperParkingStrategyChooseTargetParkingLot_ThenCanChooseParkingLotB() {
    final ParkingLot parkingLotA = new ParkingLotStub(10, "A", 2);
    final ParkingLot parkingLotB = new ParkingLotStub(10, "B", 3);
    final ParkingStrategy parkingStrategy = new SuperParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotB);
  }

  /**
   * Given Parking Lot A has 2 / 10 available space, Parking Lot B has 3 / 5 available space, Parking Lot C has 45 / 545 available space
   * When Super Parking Strategy chooses target Parking Lot
   * Then Super Parking Strategy will be able to choose Parking Lot C
   */
  @Test
  public void test_GivenParkingLotAHas2Of10AvailableSpaceAndBHas3Of5AvailableSpaceAndCHas8Of545AvailableRate_WhenSuperParkingStrategyChooseTargetParkingLot_ThenCanChooseParkingLotC() {
    final ParkingLot parkingLotA = new ParkingLotStub(10, "A", 2);
    final ParkingLot parkingLotB = new ParkingLotStub(5, "B", 3);
    final ParkingLot parkingLotC = new ParkingLotStub(45, "C", 545);
    final ParkingStrategy parkingStrategy = new SuperParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB, parkingLotC));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotC);
  }

  /**
   * Given Parking Lot A, B, C all have same available space rate
   * When Super Parking Strategy chooses target Parking Lot
   * Then Super Parking Strategy will be able to choose Parking Lot A
   */
  @Test
  public void test_GivenAllParkingLotWithSameAvailableSpace_WhenSuperParkingStrategyChooseTargetParkingLot_ThenCanChooseFirstParkingLot() {
    final ParkingLot parkingLotA = new ParkingLotStub(10, "A", 5);
    final ParkingLot parkingLotB = new ParkingLotStub(20, "B", 10);
    final ParkingLot parkingLotC = new ParkingLotStub(40, "B", 20);
    final ParkingStrategy parkingStrategy = new SuperParkingStrategy();

    final Optional<ParkingLot> targetParkingLot = parkingStrategy
        .getTargetParkingLot(Lists.newArrayList(parkingLotA, parkingLotB, parkingLotC));

    assertThat(targetParkingLot)
        .isPresent()
        .containsSame(parkingLotA);
  }
}
