package com.oobootcamp.parkinglot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

import com.oobootcamp.parkinglot.exception.CarNotFoundException;
import com.oobootcamp.parkinglot.exception.NoParkingLotException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.ParkingLotNotFoundException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;
import org.junit.Test;

public class ParkingBoyTest {

  /**
   * Given a Parking Boy manages a Parking Lot and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be able to Park a Car and get Receipt
   */
  @Test
  public void test_GivenParkingBoyWithParkingLotAndCar_WhenParkingBoyParkCar_ThenCanParkCarAndGetReceipt()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLot = TestParkingLot.LARGE_CAPACITY_DUMMY.create();
    parkingBoy.manage(parkingLot);
    final Car car = TestCar.DUMMY.create();

    final Receipt receipt = parkingBoy.park(car);

    assertThat(receipt.getCarLicence()).isEqualTo("DUMMY");
  }

  /**
   * Given a Parking Boy manages 2 empty Parking Lots and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be able to Park a Car and get Receipt
   */
  @Test
  public void test_GivenParkingBoyWith2ParkingLotsAndCar_WhenParkingBoyParkCar_ThenCanParkCarAndGetReceipt()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.LARGE_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();

    final Receipt receipt = parkingBoy.park(car);

    assertThat(receipt.getCarLicence()).isEqualTo("DUMMY");
  }

  /**
   * Given a Parking Boy manages 2 empty Parking Lots J, K and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be able to Park a Car to J and get Receipt
   */
  @Test
  public void test_GivenParkingBoyWith2AvailableParkingLotsAndCar_WhenParkingBoyParkCar_ThenCanParkCarTo1stParkingLotAndGetReceipt()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.LARGE_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();

    final Receipt receipt = parkingBoy.park(car);

    assertThat(receipt)
        .extracting("carLicence", "parkingLotName")
        .contains("DUMMY", "J");
  }

  /**
   * Given a Parking Boy manages a full Parking Lot J and Available Parking Lot K and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be able to Park a Car to K and get Receipt
   */
  @Test
  public void test_GivenParkingBoyWith1FullParkingLotAnd1AvailableParkingLotAndCar_WhenParkingBoyParkCar_ThenCanParkCarToAvailableParkingLotAndGetReceipt()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.NO_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.LARGE_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();

    final Receipt receipt = parkingBoy.park(car);

    assertThat(receipt)
        .extracting("carLicence", "parkingLotName")
        .contains("DUMMY", "K");
  }

  /**
   * Given a Parking Boy manages a full Parking Lot J and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be failed to Park a Car
   */
  @Test
  public void test_GivenParkingBoyWith1FullParkingLotAndCar_WhenParkingBoyParkCar_ThenFailedToParkCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.NO_CAPACITY_J.create();
    parkingBoy.manage(parkingLotJ);
    final Car car = TestCar.DUMMY.create();

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.park(car), ParkingLotFullException.class);

    assertThat(thrown).hasMessage("All Parking Lots are full");
  }

  /**
   * Given a Parking Boy manages a full Parking Lot J, K and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be failed to Park a Car
   */
  @Test
  public void test_GivenParkingBoyWith2FullParkingLotAndCar_WhenParkingBoyParkCar_ThenFailedToParkCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.NO_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.NO_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.park(car), ParkingLotFullException.class);

    assertThat(thrown).hasMessage("All Parking Lots are full");
  }

  /**
   * Given a Parking Boy manages a full Parking Lot J and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be failed to park the same Car again
   */
  @Test
  public void test_GivenParkingBoyWithParkingLotAndCar_WhenParkingBoyParkCar_ThenFailedToParkCarAgain()
      throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    parkingBoy.manage(parkingLotJ);
    final Car car = TestCar.DUMMY.create();
    parkingBoy.park(car);

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.park(car), SameCarParkedException.class);

    assertThat(thrown).hasMessage("Same car cannot be parked twice");
  }

  /**
   * Given a Parking Boy without Parking Lot and A Car
   * When Parking Boy park a Car
   * Then Parking Boy will be failed to park the Car
   */
  @Test
  public void test_GivenParkingBoyWithoutParkingLotAndCar_WhenParkingBoyParkCar_ThenFailedToParkCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final Car car = TestCar.DUMMY.create();

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.park(car), NoParkingLotException.class);

    assertThat(thrown).hasMessageContaining("Parking Boy has no Parking Lot");
  }

  /**
   * Given a Parking Boy manages a Parking Lot with Car and Receipt for that Car
   * When ParkingBoy pick Car using the Receipt
   * Then ParkingBoy will be able to pick the specified Car
   */
  @Test
  public void test_GivenParkingLotWithCarAndReceipt_WhenParkingBoyPickCar_ThenCanPickCorrectCar()
      throws CarNotFoundException, ParkingLotNotFoundException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    parkingBoy.manage(parkingLotJ);
    final Car car = TestCar.DUMMY.create();
    parkingLotJ.getParkedCars().add(car);
    final Receipt receipt = new Receipt("DUMMY", "J");

    final Car pickedCar = parkingBoy.pick(receipt);

    assertThat(pickedCar).isEqualTo(car);
  }

  /**
   * Given a Parking Boy manages an empty Parking Lot and Receipt for that Car
   * When ParkingBoy pick Car using the Receipt
   * Then ParkingBoy will be failed to pick the specified Car
   */
  @Test
  public void test_GivenEmptyParkingLotAndReceipt_WhenParkingBoyPickCar_ThenFailedToPickCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    parkingBoy.manage(parkingLotJ);
    final Receipt receipt = new Receipt("DUMMY", "J");

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.pick(receipt), CarNotFoundException.class);

    assertThat(thrown).hasMessage("Car is not found");
  }

  /**
   * Given a Parking Boy manages Parking Lot J with Car and Receipt for that Car but marked in Parking Lot K
   * When ParkingBoy pick Car using the Receipt
   * Then ParkingBoy will be failed to pick the specified Car
   */
  @Test
  public void test_GivenParkingLotWithCarAndWrongReceipt_WhenParkingBoyPickCar_ThenFailedToPickCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    parkingBoy.manage(parkingLotJ);
    final Car car = TestCar.DUMMY.create();
    parkingLotJ.getParkedCars().add(car);
    final Receipt receipt = new Receipt("DUMMY", "K");

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.pick(receipt), ParkingLotNotFoundException.class);

    assertThat(thrown).hasMessage("Parking Lot is not found");
  }

  /**
   * Given a Parking Boy manages 2 Parking Lot J, K with Car in K and Receipt for that Car
   * When ParkingBoy pick Car using the Receipt
   * Then ParkingBoy will be able to pick the specified Car
   */
  @Test
  public void test_Given2ParkingLotWithCarInParkingLotKAndReceipt_WhenParkingBoyPickCar_ThenCanPickCorrectCar()
      throws CarNotFoundException, ParkingLotNotFoundException {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.LARGE_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();
    parkingLotK.getParkedCars().add(car);
    final Receipt receipt = new Receipt("DUMMY", "K");

    final Car pickedCar = parkingBoy.pick(receipt);

    assertThat(pickedCar).isEqualTo(car);
  }

  /**
   * Given a Parking Boy manages 2 Parking Lot J, K with Car in J and Receipt for that Car but marked in Parking Lot K
   * When ParkingBoy pick Car using the Receipt
   * Then ParkingBoy will be failed to pick the specified Car
   */
  @Test
  public void test_Given2ParkingLotWithCarInParkingLotKAndWrongReceipt_WhenParkingBoyPickCar_ThenFailedToPickCar() {
    final ParkingBoy parkingBoy = new ParkingBoy();
    final ParkingLot parkingLotJ = TestParkingLot.LARGE_CAPACITY_J.create();
    final ParkingLot parkingLotK = TestParkingLot.LARGE_CAPACITY_K.create();
    parkingBoy.manage(parkingLotJ, parkingLotK);
    final Car car = TestCar.DUMMY.create();
    parkingLotJ.getParkedCars().add(car);
    final Receipt receipt = new Receipt("DUMMY", "K");

    final Throwable thrown = catchThrowableOfType(() -> parkingBoy.pick(receipt), CarNotFoundException.class);

    assertThat(thrown).hasMessage("Car is not found");
  }
}
