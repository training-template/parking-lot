package com.oobootcamp.parkinglot;

public class Car {

  private final String licence;

  public Car(final String licence) {
    this.licence = licence;
  }

  public String getLicence() {
    return this.licence;
  }
}
