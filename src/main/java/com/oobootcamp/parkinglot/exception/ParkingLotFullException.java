package com.oobootcamp.parkinglot.exception;

public class ParkingLotFullException extends Exception {

  public ParkingLotFullException(final String message) {
    super(message);
  }
}
