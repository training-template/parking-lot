package com.oobootcamp.parkinglot;

public class Receipt {

  private final String carLicence;
  private final String parkingLotName;

  public Receipt(final String carLicence, final String parkingLotName) {
    this.carLicence = carLicence;
    this.parkingLotName = parkingLotName;
  }

  public String getCarLicence() {
    return carLicence;
  }

  public String getParkingLotName() {
    return parkingLotName;
  }
}
