package com.oobootcamp.parkinglot.exception;

public class NoParkingLotException extends Exception {

  public NoParkingLotException(final String message) {
    super(message);
  }
}
