package com.oobootcamp.parkinglot.predicate;

import com.oobootcamp.parkinglot.Car;
import java.util.function.Predicate;

public final class CarPredicate {

  private CarPredicate() {

  }

  public static Predicate<Car> isSameLicence(final String licence) {
    return car -> licence.equals(car.getLicence());
  }
}
