package com.oobootcamp.parkinglot;

import static com.oobootcamp.parkinglot.predicate.ParkingLotPredicate.isSameName;

import com.google.common.collect.Lists;
import com.oobootcamp.parkinglot.exception.CarNotFoundException;
import com.oobootcamp.parkinglot.exception.NoParkingLotException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.ParkingLotNotFoundException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;
import com.oobootcamp.parkinglot.parkingstrategy.BasicParkingStrategy;
import com.oobootcamp.parkinglot.parkingstrategy.ParkingStrategy;
import java.util.Arrays;
import java.util.List;

public class ParkingBoy {

  private final List<ParkingLot> parkingLots;
  private final ParkingStrategy parkingStrategy;

  public ParkingBoy() {
    this(new BasicParkingStrategy());
  }

  public ParkingBoy(final ParkingStrategy parkingStrategy) {
    this.parkingStrategy = parkingStrategy;
    this.parkingLots = Lists.newArrayList();
  }

  public void manage(final ParkingLot... parkingLots) {
    this.parkingLots.addAll(Arrays.asList(parkingLots));
  }

  public Receipt park(final Car car) throws ParkingLotFullException, NoParkingLotException, SameCarParkedException {
    if (parkingLots.isEmpty()) {
      throw new NoParkingLotException("Parking Boy has no Parking Lot");
    }

    return parkingStrategy.getTargetParkingLot(parkingLots)
        .orElseThrow(() -> new ParkingLotFullException("All Parking Lots are full"))
        .park(car);
  }

  public Car pick(final Receipt receipt) throws ParkingLotNotFoundException, CarNotFoundException {
    return parkingLots.stream()
        .filter(isSameName(receipt.getParkingLotName()))
        .findFirst()
        .orElseThrow(() -> new ParkingLotNotFoundException("Parking Lot is not found"))
        .pick(receipt);
  }
}
