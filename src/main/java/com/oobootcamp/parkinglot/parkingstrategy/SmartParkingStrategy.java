package com.oobootcamp.parkinglot.parkingstrategy;

import com.oobootcamp.parkinglot.ParkingLot;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class SmartParkingStrategy implements ParkingStrategy {

  @Override
  public Optional<ParkingLot> getTargetParkingLot(List<ParkingLot> parkingLots) {
    return parkingLots.stream()
        .max(Comparator.comparingInt(ParkingLot::getAvailableSpace));
  }
}
