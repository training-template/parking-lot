package com.oobootcamp.parkinglot.parkingstrategy;

import static com.oobootcamp.parkinglot.predicate.ParkingLotPredicate.isAvailable;

import com.oobootcamp.parkinglot.ParkingLot;
import java.util.List;
import java.util.Optional;

public class BasicParkingStrategy implements ParkingStrategy {

  @Override
  public Optional<ParkingLot> getTargetParkingLot(final List<ParkingLot> parkingLots) {
    return parkingLots.stream()
        .filter(isAvailable())
        .findFirst();
  }
}
