package com.oobootcamp.parkinglot.parkingstrategy;

import com.oobootcamp.parkinglot.ParkingLot;
import java.util.List;
import java.util.Optional;

public interface ParkingStrategy {

  Optional<ParkingLot> getTargetParkingLot(final List<ParkingLot> parkingLots);
}
