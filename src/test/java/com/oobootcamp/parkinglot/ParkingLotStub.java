package com.oobootcamp.parkinglot;

import com.oobootcamp.parkinglot.exception.CarNotFoundException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;

/**
 * ParkingLotStub.
 *
 * This class is to stub available space method in order to ease different Parking Strategy
 * related to ParkingLot's max capacity and available space
 */
public class ParkingLotStub extends ParkingLot {

  private final int availableSpace;

  public ParkingLotStub(final int maxCapacity, final String name, final int availableSpace) {
    super(maxCapacity, name);
    this.availableSpace = availableSpace;
  }

  @Override
  public int getAvailableSpace() {
    return availableSpace;
  }

  /**
   * To remove the logic inside park as ParkingLotStub is not to test Parking functionality
   */
  @Override
  public Receipt park(final Car car) throws ParkingLotFullException, SameCarParkedException {
    return null;
  }

  /**
   * To remove the logic inside park as ParkingLotStub is not to test Picking functionality
   */
  @Override
  public Car pick(Receipt receipt) throws CarNotFoundException {
    return null;
  }

  @Override
  public String toString() {
    return String.format("ParkingLot: {name: %s, maxCapacity: %d, availableSpace: %d}", getName(), getMaxCapacity(),
        availableSpace);
  }
}
