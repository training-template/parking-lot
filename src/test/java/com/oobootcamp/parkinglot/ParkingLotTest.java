package com.oobootcamp.parkinglot;

import static org.assertj.core.api.Assertions.*;

import com.oobootcamp.parkinglot.exception.CarNotFoundException;
import com.oobootcamp.parkinglot.exception.ParkingLotFullException;
import com.oobootcamp.parkinglot.exception.SameCarParkedException;
import org.junit.Test;

public class ParkingLotTest {

  /**
   * Given a Parking Lot and a Car
   * When I park a Car
   * Then I will be able to park a Car
   */
  @Test
  public void test_GivenParkingLotAndCar_WhenParkCarThen_CanParkCar()
      throws ParkingLotFullException, SameCarParkedException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.DUMMY.create();

    parkingLot.park(car);

    assertThat(parkingLot.getParkedCars()).contains(car);
  }

  /**
   * Given a Parking Lot has 2 slots and 2 Cars
   * When I park 2 Cars
   * Then I will be able to park 2 Cars
   */
  @Test
  public void test_GivenParkingLotWith2SlotsAnd2Cars_WhenParkCarsThen_CanParkAllCars()
      throws ParkingLotFullException, SameCarParkedException {
    final ParkingLot parkingLot = new ParkingLot(2);
    final Car carA = TestCar.A.create();
    final Car carB = TestCar.B.create();

    parkingLot.park(carA);
    parkingLot.park(carB);

    assertThat(parkingLot.getParkedCars()).containsExactlyInAnyOrder(carA, carB);
  }

  /**
   * Given a Full Parking Lot and a Car
   * When I park a Car
   * Then I will be failed to park a Car
   */
  @Test
  public void test_GivenFullParkingLotAndCar_WhenParkCarThen_FailedToParkCar() {
    final ParkingLot parkingLot = new ParkingLot(0);
    final Car car = TestCar.DUMMY.create();

    final Throwable thrown = catchThrowableOfType(() -> parkingLot.park(car), ParkingLotFullException.class);

    assertThat(thrown).hasMessage("Parking Lot is full");
  }

  /**
   * Given a Parking Lot and a Car A
   * When I park a Car A
   * Then I will be able to park a Car A and get Receipt
   */
  @Test
  public void test_GivenParkingLotAndCarA_WhenParkCarThen_CanParkCarAndGetReceipt()
      throws ParkingLotFullException, SameCarParkedException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.A.create();

    final Receipt receipt = parkingLot.park(car);

    assertThat(receipt.getCarLicence()).isEqualTo("A");
  }

  /**
   * Given a Parking Lot and a Car B
   * When I park a Car B
   * Then I will be able to park a Car B and get Receipt
   */
  @Test
  public void test_GivenParkingLotAndCarB_WhenParkCarThen_CanParkCarAndGetReceipt()
      throws ParkingLotFullException, SameCarParkedException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.B.create();

    final Receipt receipt = parkingLot.park(car);

    assertThat(receipt.getCarLicence()).isEqualTo("B");
  }

  /**
   * Given a Parking Lot and Car
   * When I park Car
   * Then I will be failed to park the same Car again
   */
  @Test
  public void test_GivenParkingLotAndCar_WhenParkCarA_ThenFailedToParkSameCarAgain()
      throws SameCarParkedException, ParkingLotFullException {
    final ParkingLot parkingLot = new ParkingLot(2);
    final Car car = TestCar.DUMMY.create();
    parkingLot.park(car);

    final Throwable thrown = catchThrowableOfType(() -> parkingLot.park(car), SameCarParkedException.class);

    assertThat(thrown).hasMessage("Same car cannot be parked twice");
  }

  /**
   * Given a Parking Lot with Car and Receipt for that Car
   * When I pick Car using the Receipt
   * Then I will be able to pick the specified Car
   */
  @Test
  public void test_GivenParkingLotWithCarAndReceiptA_WhenPickCar_ThenCanPickCar()
      throws SameCarParkedException, ParkingLotFullException, CarNotFoundException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.DUMMY.create();
    parkingLot.park(car);
    final Receipt receipt = new Receipt("DUMMY", "");

    final Car pickedCar = parkingLot.pick(receipt);

    assertThat(pickedCar).isEqualTo(car);
  }

  /**
   * Given a Parking Lot with Car A and Car B and Receipt for Car B
   * When I pick Car B using the Receipt
   * Then I will be able to pick Car B
   */
  @Test
  public void test_GivenParkingLotWith2CarAndReceipt_WhenPickCar_ThenCanPickCorrectCar()
      throws SameCarParkedException, ParkingLotFullException, CarNotFoundException {
    final ParkingLot parkingLot = new ParkingLot(10);
    final Car carB = TestCar.B.create();
    parkingLot.park(TestCar.A.create());
    parkingLot.park(carB);
    final Receipt receipt = new Receipt("B", "");

    final Car pickedCar = parkingLot.pick(receipt);

    assertThat(pickedCar).isEqualTo(carB);
  }

  /**
   * Given an empty Parking Lot and Receipt for Car A
   * When I pick Car A using the Receipt
   * Then I will be failed to pick Car A
   */
  @Test
  public void test_GivenEmptyParkingLotAndReceipt_WhenPickCar_ThenFailedToPickCar() {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Receipt receipt = new Receipt("A", "");

    final Throwable thrown = catchThrowableOfType(() -> parkingLot.pick(receipt), CarNotFoundException.class);

    assertThat(thrown).hasMessage("Car is not found");
  }

  /**
   * Given a Parking Lot with Car and Receipt for that Car
   * When I pick that Car using the Receipt
   * Then that Car will be removed from Parking Lot
   */
  @Test
  public void test_GivenParkingLotWithCarAndReceiptA_WhenPickCar_ThenParkingLotCanRemoveTheCar()
      throws SameCarParkedException, ParkingLotFullException, CarNotFoundException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.DUMMY.create();
    parkingLot.park(car);
    final Receipt receipt = new Receipt("DUMMY", "");

    parkingLot.pick(receipt);

    assertThat(parkingLot.getParkedCars()).doesNotContain(car);
  }

  /**
   * Given a Parking Lot with Car and Receipt for that Car
   * When I pick that Car using the Receipt
   * Then that Car will be failed to be picked again from Parking Lot
   */
  @Test
  public void test_GivenParkingLotWithCarAndReceiptA_WhenPickCar_ThenFailedToPickTheSameCarTwice()
      throws SameCarParkedException, ParkingLotFullException, CarNotFoundException {
    final ParkingLot parkingLot = new ParkingLot(1);
    final Car car = TestCar.DUMMY.create();
    parkingLot.park(car);
    final Receipt receipt = new Receipt("DUMMY", "");

    parkingLot.pick(receipt);
    final Throwable thrown = catchThrowableOfType(() -> parkingLot.pick(receipt), CarNotFoundException.class);

    assertThat(thrown).hasMessage("Car is not found");
  }
}
