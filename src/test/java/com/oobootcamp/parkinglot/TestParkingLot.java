package com.oobootcamp.parkinglot;

public enum TestParkingLot {
  LARGE_CAPACITY_DUMMY(10, "DUMMY"),
  LARGE_CAPACITY_J(10, "J"),
  LARGE_CAPACITY_K(10, "K"),
  NO_CAPACITY_K(0, "J"),
  NO_CAPACITY_J(0, "K"),
  ;

  private final int maxCapacity;
  private final String parkingLotName;

  TestParkingLot(final int maxCapacity, final String parkingLotName) {
    this.maxCapacity = maxCapacity;
    this.parkingLotName = parkingLotName;
  }

  public ParkingLot create() {
    return new ParkingLot(maxCapacity, parkingLotName);
  }
}
