package com.oobootcamp.parkinglot.exception;

public class SameCarParkedException extends Exception {

  public SameCarParkedException(final String message) {
    super(message);
  }
}
